'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    prefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-clean-css'),
    less = require('gulp-less'),
    rigger = require('gulp-rigger'),
    browserSync = require("browser-sync");

var path = {
    build: { //где лежат готовые после сборки файлы
        html: 'design/build/',
        js: 'design/build/js/',
        css: 'design/build/css/',
        img: 'design/build/img/'
    },
    src: { //Пути откуда брать исходники
        html: 'design/src/*.html', 
        js: 'design/src/js/main.js',
        css: 'design/src/css/main.less',
        img: 'design/src/img/**/*.*' 
    },
    watch: { //за изменением каких файлов мы хотим наблюдать
        html: 'design/src/**/*.html',
        js: 'design/src/js/**/*.js',
        css: 'design/src/css/**/*.less',
        img: 'design/src/img/**/*.*'
    },
    clean: './design/build/**/*.*'
};

// конфигурация сервера
var config = {
    server: {
        baseDir: "./" 
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: ""
};

// сборка html
gulp.task('html:build', function (done) {
    gulp.src(path.src.html) 
        .pipe(rigger()) 
        .pipe(gulp.dest(path.build.html)); //Выплюнем их в папку build
        //.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
    done();

});

// сборкаjs
gulp.task('js:build', function (done) {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(uglify()) //Сожмем наш js
        .pipe(gulp.dest(path.build.js)); //Выплюнем готовый файл в build
       // .pipe(reload({stream: true})); //И перезагрузим сервер
    done();
});

// компиляция less в css
gulp.task('less:build', function (done) {
    gulp.src(path.src.css)
        .pipe(less())
        .pipe(prefixer())
        .pipe(cleancss())
        .pipe(gulp.dest(path.build.css)); //Выплюнем готовый файл в build
      //  .pipe(reload({stream: true})); //И перезагрузим сервер
    done();
});

// сборка картинок
gulp.task('image:build', function (done) {
    gulp.src(path.src.img) //Выберем наши картинки
        // пока не добавлены пакеты для картинок
        .pipe(gulp.dest(path.build.img)); //И бросим в build
       // .pipe(reload({stream: true}));
    done();
});

// запуск нужной задачи при изменении какого-то файла
gulp.task('watch', function(done){
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.css, gulp.series('less:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
    done();
});

// очистка папки build
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// зыдача по умолчанию, которая запускает весь процесс сборки
gulp.task('default', gulp.series('watch'));
