<?php

class Portfolio_IndexController extends ItRocks_Controller_Action  {
    
    public function indexAction() {        
        $catalogId = $this->_request->getParam('catalogId', 0);
        $itemId = $this->_request->getParam('itemId', 0);

        $portfolioModel = new Portfolio_Model_Portfolio();
        $items = $portfolioModel->getAll();


        $this->view->assign('items', $items);
        $this->view->assign('catalogId', $catalogId);
        $this->view->assign('itemId', $itemId);

//////////////////////////////////////////////////////////////////////////
        $page = $this->_request->getParam('page', 1);
        $portfolioTable = new Portfolio_Model_DbTable_Portfolio();

        $portfolio = $portfolioTable->getAll();

        $paginator = Zend_Paginator::factory($portfolio);
        $paginator->setItemCountPerPage(7);
        $paginator->setCurrentPageNumber($page);

        $this->view->assign('paginator', $paginator);
        $this->view->assign('page', $page);
    }
  
    public function portfolioAction() {
        $portfolioModel = new Portfolio_Model_Portfolio();

        $items = $portfolioModel->getAll();

        $this->view->assign('items', $items);
    }
  
    public function showAction() {
        $alias = $this->_request->getParam('alias');
        
        $table = new Portfolio_Model_Portfolio();
        $item = $table->getPortfolioByAlias($alias);     
        
//        $portfolioCommentsTable = new Portfolio_Model_DbTable_PortfolioComments();
//        $form = new Portfolio_Form_Comment();
        
//        if ($this->_request->isPost()) {
//            $formData = $this->_request->getPost();
//            if ($form->isValid($formData)) {
//                $data = array(
//                    'date' => date('Y-m-d'),
//                    'username' => $formData['username'],
//                    'email' => $formData['email'],
//                    'new' => 1,
//                    'comment' => $formData['comment'],
//                    'status' => 0,
//                    'portfolioId' => $item['id']
//                );
////                $comment = $portfolioCommentsTable->fetchNew();
////                $comment->setFromArray($data);
////                $comment->save();
//
//                $this->_helper->redirector('show', 'index', 'portfolio', array('alias' => $item['alias']));
//            }
//        }
        
        $this->view->headMeta()->setName('description', stripslashes($item['metaDescription']));
        $this->addTitle($item['metaTitle']);
        $this->view->assign('item', $item);
//        $this->view->assign('comments', $portfolioCommentsTable->getCommentsByPortfolioId($item['id']));
//        $this->view->assign('form', $form);
    }
    
    private function addTitle($title) {
        $this->view->headTitle()->setAutoEscape(false);
        $this->view->headTitle()->set(stripslashes($title));
    }
}
