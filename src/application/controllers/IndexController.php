<?php

class IndexController extends Zend_Controller_Action {

    public function mainMenuAction() {
        $action = $this->_request->getParam('action');
        $alias = $this->_request->getParam('alias');
        $module = $this->_request->getParam('module');
        $controller = $this->_request->getParam('module');

        $modules = Bootstrap::getModuleList();
        $this->view->assign('modules', $modules);

        $this->view->assign('action', $action);
        $this->view->assign('controller', $controller);
        $this->view->assign('module', $module);
        $this->view->assign('alias', $alias);
    }

    public function headerAction() {
        $settingsModel = new Model_Settings();
        $data = $settingsModel->getSettings(['phone', 'address']);
        $this->view->assign('data', $data);

        $action = $this->_request->getParam('action');
        $alias = $this->_request->getParam('alias'); //contacts about в модуле static
        $module = $this->_request->getParam('module');
        $controller = $this->_request->getParam('module');

        $modules = Bootstrap::getModuleList();
        $this->view->assign('modules', $modules);

        $this->view->assign('action', $action);
        $this->view->assign('controller', $controller);
        $this->view->assign('module', $module);
        $this->view->assign('alias', $alias);
    }

    public function socialIconsAction() {
        $settingsModel = new Model_Settings();
        $social     = $settingsModel->getSettings(['vk','facebook', 'twitter', 'odnoklassniki', 'instagram']);
        $this->view->assign('social', $social);
    }

    public function contactsFooterAction() {
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['address', 'phone', 'email']);

        $this->view->assign('contacts', $contacts);
    }

    public function contactsBlockAction() {
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['address', 'phone', 'email']);

        $this->view->assign('contacts', $contacts);
    }

    public function headerNavTopAction(){
        $settingsModel = new Model_Settings();
        $contacts     = $settingsModel->getSettings(['phone']);

        $this->view->assign('contacts', $contacts);
    }
   
    public function searchAction() {
        $modules = Bootstrap::getModuleList();
        $searchModel = new Model_Search();
        $request = trim($this->getParam('query'));
        $buttonTitle = "buttonSearchTitle";
        $items = [];
        $errorString = "";

        if ($this->_request->isGet()) {
                $buttonTitle = "buttonSearchTryAgain";
                if (strlen($request) > 0) {

                    $items = $searchModel->search($request);
                    if (empty($items)) {
                        $errorString = "resultsNotFound";
                    }
                } else {
                    $errorString = "minimumCharInStringSearch";
                }
        }

        $this->view->assign('modules', $modules);
        $this->view->assign('searchQuery', $request);
        $this->view->assign('searchTitle', $buttonTitle);
        $this->view->assign('items', $items);
        $this->view->assign('errorString', $errorString);
    }

    public function feedbackAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        try{
            if ($this->_request->isPost()) {
                $formData = $this->_request->getPost();
                $name = $formData['name'];
                $phone = $formData['phone'];
//                $message =  $formData['message'];
                if (($this->nameValidation($name)) && ($this->phoneValidation($phone))) {
                    $date = date('Y-m-d H:i:s');
                    $feedbackTable = new Static_Model_DbTable_Feedback();

                    if (isset($formData['message'])){
                        $feedback = $feedbackTable->createRow([
                            'phone' => $phone,
                            'name' => $name,
                            'date' => $date,
                            'message' => $formData['message']
                        ]);
                    }
                    else{
                        $feedback = $feedbackTable->createRow([
                            'phone' => $phone,
                            'name' => $name,
                            'date' => $date,
                        ]);
                    }
//
//
//                    $feedback = $feedbackTable->createRow([
//                        'phone' => $phone,
//                        'name' => $name,
//                        'date' => $date,
//                        'message' => $message
//                    ]);

                    $feedback->save();
                    $modelMail = new Model_Mail();

                    $modelMail->sendNewRequest($phone, $name, $date, "Пришла заявка");

                    echo json_encode(['success' => 'success']);
                } else {
                    echo json_encode(['error' => 'Ошибка! Проверьте введенные данные']);
                }
            }
        }
        catch (Exception $exception) {
            echo json_encode(['error' => $exception->getMessage()]);
        }
    }

    public function feedbackSuccessAction(){}

    public function footerAction() {
        $settingsModel = new Model_Settings();
        $data = $settingsModel->getSettings(['yandexMetrika', 'googleAnalytics', 'address', 'phone', 'email']);

        $this->view->assign('data', $data);
    }

    public function sliderAction(){
        $sliderTable = new Static_Model_DbTable_Slider();
        $images = $sliderTable->getSliderImages();
        $this->view->assign('images', $images);
    }

    private function nameValidation($name) {
        return preg_match("/^[а-яА-ЯёЁ\s]+$/u", $name) ? true : false;
    }

    private function phoneValidation($phone) {
        return preg_match("/[0-9\-\+\s]{6,}$/", $phone) ? true : false;
    }

    public function createRequestAction(){}
    public function publicityAction(){}
    public function iconsAction(){}
    public function questionFormAction(){}

    public function callModalAction(){}

    public function thanksModalAction(){}
}