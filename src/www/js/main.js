$(function() {
    $(".slider").slick({
            dots: !0,
            infinite: !0,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: !0,
            autoplaySpeed: 700
        }
    ),
    $(".achievements-slider").slick({
        dots: !0,
        infinite: !0,
        slidesToShow: 4,
        centerMode: !0,
        slidesToScroll: 1,
        variableWidth: !0,
        autoplay: !0,
        autoplaySpeed: 700
    });


    $(document).ready(function () {
        $('#call-modal-sending').click(function () {
            $.ajax({
                type: "post",
                url: config.callModal.request,
                dataType: 'json',
                data: {
                    name: $('#modal-input-name').val(),
                    phone: $('#modal-input-phone').val()
                },
                success: function (response) {
                    var responceElem = $('#feedback-responce');
                    if (response.success){
                        // responceElem.html('Форма отправлена!');
                        // ставим таймер на модальное окно звонка
                        setTimeout(function () {
                            $('#callModal').modal('hide');
                            $('#thanksModal').modal('show'); // вызываем новую модалку
                        }, 2000);
                        // //таймер на второю модалку
                        setTimeout(function () {
                            $('#thanksModal').modal('hide');
                        }, 6000);
                    }
                    else {
                        if (response.error) {
                            responceElem.html('Ошибка. Проверьте введенные данные')
                        }
                    }
                },
                error: function (errors) {
                    $('#feedback-responce').html('Ошибка! все плохо');
                }
            });
            return false;
        });

        $('#create-request-sending').click(function () {
            $.ajax({
                type: "post",
                url: config.callModal.request,
                dataType: 'json',
                data: {
                    name: $('#request-input-name').val(),
                    phone: $('#request-input-phone').val()
                },
                success: function (response) {
                    var responceElem = $('#feedback-responce');
                    if (response.success){
                        // responceElem.html('Форма отправлена!');
                        $('#thanksModal').modal('show'); // вызываем новую модалку
                        // //таймер на второю модалку
                        setTimeout(function () {
                            $('#thanksModal').modal('hide');
                        }, 6000);
                    }
                    else {
                        if (response.error) {
                            responceElem.html('Ошибка. response.error')
                        }
                    }
                },
                error: function (errors) {
                    $('#feedback-responce').html('Ошибка! все плохо');
                }
            });
            return false;
        });

        $('#publicity-sending').click(function () {
            $.ajax({
                type: "post",
                url: config.callModal.request,
                dataType: 'json',
                data: {
                    name: $('#publicity-input-name').val(),
                    phone: $('#publicity-input-phone').val()
                },
                success: function (response) {
                    var responceElem = $('#feedback-responce');
                    if (response.success){
                        responceElem.html('Форма отправлена!');
                        $('#thanksModal').modal('show'); // вызываем новую модалку
                        // //таймер на второю модалку
                        setTimeout(function () {
                            $('#thanksModal').modal('hide');
                        }, 6000);
                    }
                    else {
                        if (response.error) {
                            responceElem.html('Ошибка. response.error')
                        }
                    }
                },
                error: function (errors) {
                    $('#feedback-responce').html('Ошибка! все плохо');
                }
            });
            return false;
        });

        $('#question-sending').click(function () {
            $.ajax({
                type: "post",
                url: config.callModal.request,
                dataType: 'json',
                data: {
                    name: $('#question-input-name').val(),
                    phone: $('#question-input-phone').val(),
                    message: $('.question-message')
                },
                success: function (response) {
                    var responceElem = $('#feedback-responce');
                    if (response.success){
                        $('#thanksModal').modal('show'); // вызываем новую модалку
                        // //таймер на второю модалку
                        setTimeout(function () {
                            $('#thanksModal').modal('hide');
                        }, 6000);
                    }
                    else {
                        if (response.error) {
                            responceElem.html('Ошибка. response.error')
                        }
                    }
                },
                error: function (errors) {
                    $('#feedback-responce').html('Ошибка! все плохо');
                }
            });
            return false;
        });

        $('#review-sending').click(function () {
            $.ajax({
                type: "post",
                url: config.callModal.request,
                dataType: 'json',
                data: {
                    name: $('#review-input-name').val(),
                    phone: $('#review-input-phone').val(),
                    message: $('#review-message')
                },
                success: function (response) {
                    var responceElem = $('#feedback-responce');
                    if (response.success){
                        $('#thanksModal').modal('show'); // вызываем новую модалку
                        // //таймер на второю модалку
                        setTimeout(function () {
                            $('#thanksModal').modal('hide');
                        }, 6000);
                    }
                    else {
                        if (response.error) {
                            responceElem.html('Ошибка. response.error')
                        }
                    }
                },
                error: function (errors) {
                    $('#feedback-responce').html('Ошибка! все плохо');
                }
            });
            return false;
        });
    });

});




////////////////////////////
//     $.ajax({
//         url:config.hotelroom.request,
//         method:"POST",
//         data:{
//             room:o,phone:r,fio:i},
//         dataType:"json",
//         success:function(o){
//             void 0!==o.success?location.href=config.hotelroom.successUrl:
//                 void 0!==o.error && (void 0!==o.error.fio && $("#booking-fio-error").html(o.error.fio),
//                 void 0!==o.error.phone&&$("#booking-phone-error").html(o.error.phone))},
//         error:function(o){
//             return $("#booking-common-error").html("Произошла непридвиненная ошибка. Приносим извинения"),!1
//         }
//     })
//
//     $.ajax({
//         url:config.hotelroom.changeCategoryUrl,
//         method:"GET",data:{alias:i},
//         dataType:"HTML",
//         success:function(i){
//             var e=Math.round((o-1)*r+r/3+5);
//             return $(".rooms-list-container").html(i),$(".room-list-category-indicator").css("left",e+"%"),!1},
//         error:function(o){
//             return alert("Произошла ошибка на стороне сервере. Приносим извинения")
//         }
//     })
//
//
//     $.ajax({
//         url:config.hotelroom.request,
//         method:"POST",
//         data:{phone:o,fio:r},
//         dataType:"json",success:function(o){void 0!==o.success?location.href=config.hotelroom.successUrl:void 0!==o.error&&(void 0!==o.error.fio&&$("#slider-fio-error").html(o.error.fio),void 0!==o.error.phone&&$("#slider-phone-error").html(o.error.phone))},error:function(o){return $("#booking-common-error").html("Произошла непридвиненная ошибка. Приносим извинения"),!1}}),!1}),$(".index-contacts-content-btn").click(function(){return $("#bookingModal").modal("toggle"),!1});
//
//
